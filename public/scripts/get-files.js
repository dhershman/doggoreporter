$(function() {
	function getFiles() {
		$.ajax({
				url: '/resource/get-files',
				type: 'GET',
				processData: true,
				success: function(d) {
					$.get('/assets/templates/gallery.hbs', function(resp) {
						var template = Handlebars.compile(resp);
						var html = template({ data: d });
						$('.img-gallery').html(html);
						$('.main-gallery ul').bsPhotoGallery({
							'classes': 'col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12',
							'hasModal': true
						});
					});
				},
				error: function(e) {
					console.log(e);
					throw 'Error on getting image';
				}
			});
	}
	getFiles();
});