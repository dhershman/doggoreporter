$(function() {
	function uploadFile() {
		var $page = $('#imagesContent');
		var $uploadBtn = $page.find('#uploadFile');

		$uploadBtn.on('click', function(e) {
			$page.find('#upload-input').click();
		});

		$page.on('change', '#upload-input', startUpload);

		function startUpload(e) {
			var formData = new FormData();
			var files = $(e.currentTarget).get(0).files;
			if (files.length > 0) {
				if (files.length > 1) {
					for (var i = 0; i < files.length; i++) {
						formData.append('imgFile', files[i]);
						formData.append('filename', files[i].name);
						formData.append('type', files[i].type);
					}
				} else {
					formData.append('imgFile', files[0]);
					formData.append('filename', files[0].name);
					formData.append('type', files[0].type);
				}
				upload(formData);
			}
		}

		function upload(args) {
			$.ajax({
				url: '/resource/upload-file',
				type: 'POST',
				data: args,
				processData: false,
				contentType: false,
				success: function(d) {
					console.log(d);
				},
				error: function(e) {
					console.log(e);
					throw 'Error on uploading image';
				}
			});
		}
	}

	uploadFile();
});