$(function() {
	function doggoReports() {
		var $page = $('.reports-content');
		var $chart = $page.find('#mainChart');
		var $tableEls = $page.find('#doggoTable');
		var chartHidden = true;
		var reportData = [];
		$chart.hide();

		$.get('/assets/templates/reportsTable.hbs', function(resp) {
			var template = Handlebars.compile(resp);
			$.ajax({
				type: 'GET',
				async: true,
				url: '/resource/get-report',
				data: { date: new Date() },
				success: function(d) {
					var parsedData = JSON.parse(d);
					getThisWeeksData(parsedData);
					var html = template({ data: parsedData });
					render(parsedData);
				},
				error: function(err) {
					throw 'Error on obtaining data';
				}
			});
		});

		function render(d) {
			$tableEls.find('#reportsTable').DataTable({
				data: d,
				columns: [
					{data: 'cleanDate'},
					{data: 'name'},
					{data: foodMerger},
					{data: medsMerger},
					{data: 'doggo_info.activity_notes'},
					{data: 'doggo_info.behaviour_notes'}
				]
			});
			// $tableEls.html(html);
		}

		function foodMerger(data, type, dataToSet) {
			return data.doggo_info.food_amt + '<br>' + data.doggo_info.food_time;
		}

		function medsMerger(data, type, dataToSet) {
			return data.doggo_info.medicine_amt + '<br>' + data.doggo_info.medicine_time;
		}

		function getThisWeeksData(d) {
			var sortStyle = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
			var week = startAndEndOfWeek();
			for (var i = 0; i < d.length; i++) {
				var dataDate = d[i].cleanDate.split('/')[1];
				if (dataDate >= week[0].getDate() && dataDate <= week[1].getDate()) {
					reportData[sortStyle.indexOf(d[i].day)] = Number(d[i].doggo_info.activity_hours);
				}
			}
		}

		function startAndEndOfWeek() {
			var d = new Date();

			d.setHours(0,0,0,0);
			var monday = new Date(d);
			monday.setDate(monday.getDate() - monday.getDay() + 1);

			var sunday = new Date(d);
			sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
			return [monday, sunday];
		}

		$page.on('click', '.js-load-activity-chart', function(e) {
			if (chartHidden) {
				$tableEls.fadeOut('fast');
				$chart.fadeIn('fast');
				$(this).text('Show Table');
				chartRender();
				chartHidden = false;
			} else {
				$chart.fadeOut('fast');
				$tableEls.fadeIn('fast');
				$(this).text('Show Activity Chart');
				chartHidden = true;
			}
		});

		function chartRender() {
			var lineChart = new Chart($chart, {
				type: 'bar',
				data: {
					labels: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
					datasets: [{
						label: 'Activity Hours',
						backgroundColor: [
							'rgba(255, 99, 132, 0.2)',
							'rgba(54, 162, 235, 0.2)',
							'rgba(255, 206, 86, 0.2)',
							'rgba(75, 192, 192, 0.2)',
							'rgba(153, 102, 255, 0.2)',
							'rgba(255, 159, 64, 0.2)'
						],
						borderColor: [
							'rgba(255,99,132,1)',
							'rgba(54, 162, 235, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(75, 192, 192, 1)',
							'rgba(153, 102, 255, 1)',
							'rgba(255, 159, 64, 1)'
						],
						borderWidth: 1,
						data: reportData
					}]
				},
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			});
		}
	}


	doggoReports();
});
