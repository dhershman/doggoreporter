$(function() {
	function doggoHome() {
		var $page = $('.main-content');
		var $meds = $page.find('.js-doggo-medicine');

		$page.on('change', '.js-which-doggo', doggoSelected);

		function doggoSelected(e) {
			var doggo	= $(e.currentTarget).val();
			if (doggo.toLowerCase() === 'echo') {
				$meds.fadeIn('fast');
			} else {
				$meds.fadeOut('fast');
			}
		}
	}

	doggoHome();
});