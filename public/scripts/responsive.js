$(function() {
  var currWidth = $(document).width();
  var cellWidth = 980;
  var responsiveMode = false;
  if (currWidth <= cellWidth) {
    responsiveMode = true;
    swapBtn();
  }
  $(window).resize(function() {
      responsiveMode = ($(this).width() <= cellWidth);
      swapBtn();
  });

  function swapBtn() {
    if(responsiveMode) {
        $('.js-main').addClass('hidden');
        $('.js-responsive').removeClass('hidden');
    } else {
      $('.js-main').removeClass('hidden');
      $('.js-responsive').addClass('hidden');
    }
  }

});
