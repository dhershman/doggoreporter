'use strict';

const hapi = require('hapi');
const path = require('path');

const server = new hapi.Server();

server.connection({
	port: 4500
});

server.register([{
		register: require('hapi-rethinkdb'),
		options: {
			url: 'rethinkdb://localhost:28015/doggoreport'
		}
	},
	require('vision'),
	require('inert'),
	require('./lib'),
	require('./src/app/landing')
], (err) => {
	if (err) throw err;

	server.route({
		method: 'GET',
		path: '/assets/{param*}',
		config: {
			auth: false
		},
		handler: {
			directory: {
				path: path.join(__dirname, 'public')
			}
		}
	});

	server.views({
		engines: {
			hbs: require('handlebars')
		},
		path: './src/app',
		layoutPath: './src/templates/layouts',
		helpersPath: './src/templates/helpers',
		partialsPath: './src/templates/partials',
		layout: 'main'
	});

	server.start((err) => {
		if (err) throw err;
		console.log('Server running at: ' + server.info.uri);
		server.log('info', 'Server running at: ' + server.info.uri);
	});

});
