'use strict';

exports.register = (server, options, next) => {
	let r = server.plugins['hapi-rethinkdb'].rethinkdb;
	let conn = server.plugins['hapi-rethinkdb'].connection;
	//Verify our db exists I will need to find a better way to achieve this
	r.dbList().run(conn, (err, res) => {
		if (res.indexOf('doggoreport' === -1)) {
			r.dbCreate('doggoreport').run(conn, (err, res) => {
				r.db('doggoreport').tableCreate('doggo_info').run(conn, (err, res) => {
					r.db('doggoreport').tableCreate('doggo_assets').run(conn, (err, res) => {
						return;
					});
				});
			});
		}
	})
	server.route({
		path: '/resource/upload-file',
		method: 'POST',
		config: {
			payload: {
				output: 'stream',
				parse: true,
				allow: 'multipart/form-data'
			}
		},
		handler: (request, reply) => {
			return require('./resources/upload-file')(request, reply);
		}
	});

	server.route({
		path: '/resource/{path*}',
		method: ['GET', 'POST'],
		handler: (request, reply) => {
			return require('./resources/' + request.params.path)(request, reply);
		}
	});

	next();

};

exports.register.attributes = {
	pkg: require('./package.json')
};
