'use strict';

const boom = require('boom');
const moment = require('moment');

module.exports = (request, reply) => {

	let r = request.server.plugins['hapi-rethinkdb'].rethinkdb;
	let conn = request.server.plugins['hapi-rethinkdb'].connection;
	let date = moment(new Date()).format('MM/DD/YYYY');
	let reportTime = '[' + moment(new Date()).format('hh:mm:ss') + ']:';
	let reportDay = moment(new Date()).format('dddd');
	let payload = request.payload;

	r.table('doggo_info').filter({cleanDate: date, name: payload.doggo}).run(conn, (err, cursor) => {
		if (err) throw boom.badImplementation('Failed to connect to the Database');
		cursor.toArray((err, results) => {

			if (results[0]) {
				if (!payload.medicine_amt) payload.medicine_amt = 'N/A';
				if (payload.activity_hours) payload.activity_hours = Number(payload.activity_hours) + Number(results[0].doggo_info.activity_hours);
				if (payload.activity_minutes)  payload.activity_minutes = Number(payload.activity_minutes) + Number(results[0].doggo_info.activity_minutes);
				if (payload.behaviour_notes) {
					if (results[0].doggo_info.behaviour_notes) payload.behaviour_notes = results[0].doggo_info.behaviour_notes + ', ' + reportTime + payload.behaviour_notes;
				}
				if (payload.activity_notes) {
					if (results[0].doggo_info.activity_notes) payload.activity_notes = results[0].doggo_info.activity_notes + ', ' + reportTime + payload.activity_notes;
				}
				if (payload.food_time !== results[0].doggo_info.food_time) payload.food_time = 'AM & PM';
				if (payload.medicine_time && payload.medicine_time !== results[0].doggo_info.medicine_time) payload.medicine_time = 'AM & PM';
				r.table('doggo_info').filter({cleanDate: date, name: payload.doggo}).update({doggo_info: payload}).run(conn, (err, cursor) => {
					if (err) throw boom.badImplementation('Failed to update entry in database', err);
					return reply.redirect('/success');
				});

			} else {
				r.table('doggo_info').insert({
					name: payload.doggo,
					cleanDate: date,
					date: new Date(),
					doggo_info: payload,
					day: reportDay
				}).run(conn, (err, cursor) => {
					if (err) throw boom.badImplementation('Failed to submit report to database', err);
					return reply.redirect('/success');
				});
			}
		});
	});
};