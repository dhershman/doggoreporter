'use strict';

const boom = require('boom');

module.exports = (request, reply) => {
	let r = request.server.plugins['hapi-rethinkdb'].rethinkdb;
	let conn = request.server.plugins['hapi-rethinkdb'].connection;

	r.table('doggo_info').run(conn, (err, cursor) => {
		cursor.toArray((err, result) => {
			if (err) throw boom.badImplementation('Unable to obtain payload from table');
			return reply(JSON.stringify(result));
		});
	});
};