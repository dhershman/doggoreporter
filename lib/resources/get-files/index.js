'use strict';

const boom = require('boom');

module.exports = (request, reply) => {
	let r = request.server.plugins['hapi-rethinkdb'].rethinkdb;
	let conn = request.server.plugins['hapi-rethinkdb'].connection;

	r.table('doggo_assets').filter({type: 'image/jpeg'}).pluck('filename', 'path').run(conn, (err, cursor) => {
		if (err) throw boom.badImplementation('Unable to find files');
		cursor.toArray((err, results) => {
			if (err) throw boom.badImplementation('Unable to read files');
			if (results.length > 0) {
				return reply(results);
			}
		});
	});

};
