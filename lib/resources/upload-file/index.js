'use strict';

const boom = require('boom');
const path = require('path');
const fs = require('fs');

module.exports = (request, reply) => {
	let r = request.server.plugins['hapi-rethinkdb'].rethinkdb;
	let conn = request.server.plugins['hapi-rethinkdb'].connection;

	let payload = request.payload;
	let uploadDir = './public/imgs/uploads';

	let currentCount = fs.readdirSync(uploadDir);
	let name = payload.filename;

	let tmp = name.split('.');
	if (currentCount.length > 0) {
		name = tmp[0] + (currentCount.length+1) + '.' + tmp[1];
	} else {
		name = tmp[0] +0 +  '.' + tmp[1];
	}

	fs.writeFile(path.join(uploadDir, name), payload.imgFile._data, (err) => {
		if (err) throw boom.badImplementation('Unable to upload file');
		r.table('doggo_assets').insert({
			type: payload.type,
			originalFileName: payload.filename,
			filename: name,
			path: '/assets/imgs/uploads/' + name,
			file: payload.imgFile._data
		}).run(conn, (err, results) => {
			if (err) throw boom.badImplementation('Unable to read file');
			return reply(results);
		});
	});


};
