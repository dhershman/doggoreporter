'use strict';
const Handlebars = require('handlebars');

if (!global.utils) global.utils = {};


exports.getTemplate = function(path, done) {
	if (global.utils.templateCache == null) {
		global.utils.templateCache = {};
	}
	if (global.utils.templateCache[path] != null) {
		done(global.utils.templateCache[path]);
	} else {
		$.get(path, function(contents) {
			global.utils.templateCache[path] = Handlebars.compile(contents);
			done(global.utils.templateCache[path]);
		});
	}
};
