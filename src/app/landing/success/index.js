'use strict';

module.exports = (request, reply) => {
	reply.view('landing/success/index', {
		js: {
			scripts: ['doggoHome', 'responsive']
		}
	});
};
