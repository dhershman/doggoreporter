'use strict';

module.exports = (request, reply) => {

	reply.view('landing/coming-soon/index', {
		soon_active: true
	});
};