'use strict';

module.exports = (request, reply) => {

	reply.view('landing/images/index', {
		i_active: true,
		css: {
			vendor_noMin: ['bootstrap-photo-gallery']
		},
		js: {
			scripts: ['upload-file', 'get-files', 'responsive'],
			vendor: ['handlebars'],
			vendor_noMin: ['bootstrap-photo-gallery']
		}
	});
};
