'use strict';

module.exports = (request, reply) => {
	reply.view('landing/error/index', {
		js: {
			scripts: ['submit']
		},
		css: {
			page: 'css/landing/home/index.css'
		}
	});
};