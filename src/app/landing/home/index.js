'use strict';

module.exports = (request, reply) => {
	reply.view('landing/home/index', {
		form_active: true,
		js: {
			scripts: ['doggohome', 'responsive']
		},
		css: {
			page: 'css/landing/home/index.css'
		}
	});
};
