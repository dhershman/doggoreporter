'use strict';

module.exports = (request, reply) => {
	reply.view('landing/videos/index', {
		v_active: true
	});
};