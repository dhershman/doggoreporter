'use strict';

exports.register = (server, options, next) => {
	server.route({
		path: '/',
		method: 'GET',
		handler: (request, reply) => {
			return require('./home')(request, reply);
		}
	});

	server.route({
		path: '/images',
		method: 'GET',
		handler: (request, reply) => {
			return require('./images')(request, reply);
		}
	});

	server.route({
		path: '/videos',
		method: 'GET',
		handler: (request, reply) => {
			return require('./videos')(request, reply);
		}
	});

	server.route({
		path: '/error',
		method: 'GET',
		handler: (request, reply) => {
			return require('./error')(request, reply);
		}
	});

	server.route({
		path: '/coming-soon',
		method: 'GET',
		handler: (request, reply) => {
			return require('./coming-soon')(request, reply);
		}
	});

	server.route({
		path: '/reports',
		method: 'GET',
		handler: (request, reply) => {
			return require('./reports')(request, reply);
		}
	});

	server.route({
		path: '/success',
		method: 'GET',
		handler: (request, reply) => {
			return require('./success')(request, reply);
		}
	});

	next();
};

exports.register.attributes = {
	pkg: require('./package.json')
};