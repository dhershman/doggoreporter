'use strict';

module.exports = (request, reply) => {

	reply.view('landing/reports/index', {
		r_active: true,
		page: {
			useDataTables: true
		},
		js: {
			scripts: ['reports', 'responsive'],
			vendor: ['handlebars']
		}
	});
};
